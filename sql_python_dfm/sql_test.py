
# https://datatofish.com/how-to-connect-python-to-sql-server-using-pyodbc/
# https://stackoverflow.com/questions/25475906/set-ulimit-c-from-outside-shell
# https://github.com/mkleehammer/pyodbc/wiki/Getting-started

import pyodbc
import os
import sys

#sys.path.append('/opt/oracle/instantclient_19_8/libsqora.so.19.1')

#print(os.environ.get('TNS_ADMIN'))
#print(os.environ.get('LD_LIBRARY_PATH'))
#LD_LIBRARY_PATH = ('/opt/oracle/instantclient_19_8')
#os.environ['TNS_ADMIN'] = "/opt/oracle/instantclient_19_8"
#os.environ['LD_LIBRARY_PATH'] = "/opt/oracle/instantclient_19_8:%s"%LD_LIBRARY_PATH

#print(os.environ.get('TNS_ADMIN'))
#print(os.environ.get('LD_LIBRARY_PATH'))

#export TNS_ADMIN=/opt/oracle/instantclient_19_8
#export LD_LIBRARY_PATH=/opt/oracle/instantclient_19_8:${LD_LIBRARY_PATH}

def dictMCADB(target='MCADB.XFEL_VIEW_MAGNETS'):

    conn = pyodbc.connect('DSN=MCADB;PWD=ora#db4MCA')
    cursor = conn.cursor()
    cursor.execute("SELECT * FROM %s"%target)

    columns = [column[0] for column in cursor.description]
    rows = cursor.fetchone()
    
    dict_data = {}

    for i, column in enumerate(columns):

        dict_data[column] = rows[i]


def dictMCADB_all(target='MCADB.XFEL_VIEW_MAGNETS'):

    conn = pyodbc.connect('DSN=MCADB;PWD=ora#db4MCA')
    cursor = conn.cursor()
    cursor.execute("SELECT * FROM %s"%target)

    columns = [column[0] for column in cursor.description]
    rows = cursor.fetchall()

    dict_data = {}

    for j, row in enumerate(rows):

        dict_in = {}

        for i, column in enumerate(columns):

            dict_in[column] = rows[i]

        dict_data[row[0]] = dict_in

    return dict_data


def deliverCursor(target='MCADB.XFEL_VIEW_MAGNETS'):

    conn = pyodbc.connect('DSN=MCADB;PWD=ora#db4MCA')
    cursor = conn.cursor()
    cursor.execute("SELECT * FROM %s"%target)
 

    columns = [column[0] for column in cursor.description]
    rows = cursor.fetchall()

    #print(columns)
    #print(rows[0])
    return columns, rows


def returnDict(target):

    columns, rows = deliverCursor(target)

    print(columns, rows[0])
    
    dict_data = {}

    for row in rows:
        
        identifier = row[0]

        dict_in = {}

        if len(row) == len(columns):
        
            for i, column in enumerate(columns):

                dict_in[column] = row[i]
        
        else:

            print('\nError. Columns and Rows not matching\n')


        dict_data[identifier] = dict_in


    return dict_data











#for i in row:
#    print(i)


if __name__ == '__main__':
   
    list_views = ['MCADB.XFEL_VIEW_MAGNETS','MCADB.XFEL_VIEW_COMPONENTS','MCADB.VXFEL_VIEW_MAGNETS','MCADB.VXFEL_VIEW_COMPONENTS']

    print('\nREMEMBER\n')
    print('source /export/doocs/server/magnet_ml_server/ENVIRONMENT')
    try:
        
        #data = (dictMCADB_all())
        #print(type(data))
        #print(len(data))
        #print(data.keys())
        
        #for view in list_views:
        #    deliverCursor(view)

        #print(data[0])
        #print()
        #print(data[1])
        #print(data[1005])
        #print(data[1006])

        dict_test = returnDict(list_views[0])

        print(dict_test)

        #for element in data:
            #print(len(element))

    except pyodbc.Error:
        print(pyodbc.Error())
        #raise
       
