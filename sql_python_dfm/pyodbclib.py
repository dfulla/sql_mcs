
import pyodbc
import os
import sys

'''
This library delivers a python dictionary containing the data of an MCADB view.
Instructions:
- In a server with access to MCADB:
    - source /export/doocs/server/magnet_ml_server/ENVIRONMENT
- ipython (in the same directory of the library or include the path with sys.path.append()
- from pyodbclib import dictMCADB as DM
- dictName = DM('[200~MCADB.XFEL_VIEW_MAGNETS')
- print(dictName.keys())

'''


def deliverCursor(target='MCADB.XFEL_VIEW_MAGNETS'):

    conn = pyodbc.connect('DSN=MCADB;PWD=ora#db4MCA')
    cursor = conn.cursor()
    cursor.execute("SELECT * FROM %s"%target)

    columns = [column[0] for column in cursor.description]
    rows = cursor.fetchall()

    return columns, rows

def returnDict(target):

    columns, rows = deliverCursor(target)
    dict_data = {}

    for row in rows:

        identifier = row[0]

        dict_in = {}

        if len(row) == len(columns):

            for i, column in enumerate(columns):

                dict_in[column] = row[i]
     
        else:

            print('\nError. Columns and Rows not matching\n')

        dict_data[identifier] = dict_in

    return dict_data

def dictMCADB(target):

    try:

        dict_data = returnDict(target)
        return dict_data

    except:

        if os.environ.get('LD_LIBRARY_PATH') != '/opt/oracle/instantclient_19_8:':
            print('\nREMEMBER to source before using the pyodbclib library.\n')
            print('source /export/doocs/server/magnet_ml_server/ENVIRONMENT')
