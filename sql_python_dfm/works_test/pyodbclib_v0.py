import pyodbc
import os
import sys

def dictMCADB(target='MCADB.XFEL_VIEW_MAGNETS'):

    try:
        conn = pyodbc.connect('DSN=MCADB;PWD=ora#db4MCA')
        cursor = conn.cursor()
        cursor.execute("SELECT * FROM %s"%target)

        columns = [column[0] for column in cursor.description]
        rows = cursor.fetchall()

        dict_data = {}

        for j, row in enumerate(rows):

            dict_in = {}

            for i, column in enumerate(columns):

                dict_in[column] = rows[i]

            dict_data[row[0]] = dict_in

        return dict_data

    except:

        if os.environ.get('LD_LIBRARY_PATH') != '/opt/oracle/instantclient_19_8:':
            print('\nREMEMBER to source before using the library.\n')
            print('source /export/doocs/server/magnet_ml_server/ENVIRONMENT')

